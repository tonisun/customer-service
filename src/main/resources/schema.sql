CREATE TABLE customer (
    id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    street varchar(100) NOT NULL,
    zip varchar(10) NOT NULL,
    city varchar(50) NOT NULL,
    email varchar(100)
);