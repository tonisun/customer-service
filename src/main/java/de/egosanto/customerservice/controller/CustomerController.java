package de.egosanto.customerservice.controller;

import com.fasterxml.jackson.annotation.JsonBackReference;
import de.egosanto.customerservice.model.Customer;
import de.egosanto.customerservice.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;
    
    @GetMapping("/")
    public List<Customer> getAllCustomers(){
        List<Customer> customersList = new ArrayList<>();
        Iterable<Customer> customers =  customerRepository.findAll();

        for (Customer customer : customers)
            customersList.add(customer);

        return customersList;
    }

    @PostMapping("/addCustomer")
    @JsonBackReference
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        // save article in db
        return new ResponseEntity<Customer>(customerRepository.save(customer), HttpStatus.OK);
    }

    @GetMapping("/getCustomer")
    public ResponseEntity<Customer> getCustomer(@RequestParam(value = "id") Long id){
        // get article by id from db
        Optional<Customer> customerFromDB = customerRepository.findById(id);

        if (customerFromDB.isPresent())
            return new ResponseEntity<Customer>(customerFromDB.get(), HttpStatus.OK);
        else
            return new ResponseEntity("No Customer found with this id " + id, HttpStatus.NOT_FOUND);
    }

    @PutMapping("/updateCustomer")
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) {
        Optional<Customer> customerFromBody = customerRepository.findById(customer.getId());

        if (customerFromBody.isPresent()) {
            Customer savedCustomer = customerRepository.save(customer);
            return new ResponseEntity<Customer>(savedCustomer, HttpStatus.OK);
        }

        return new ResponseEntity("No customer to update found with id " + customer.getId(), HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("/deleteCustomer")
    public ResponseEntity deleteCustomer(@RequestParam(value = "id") Long id) {
        Optional<Customer> customerFromDB = customerRepository.findById(id);
        if (customerFromDB.isPresent()) {
            customerRepository.deleteById(id);
            return new ResponseEntity("Customer deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity("No customer found with this id " + id, HttpStatus.NOT_FOUND);
        }
    }

}
