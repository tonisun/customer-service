package de.egosanto.customerservice;

import de.egosanto.customerservice.model.Customer;
import de.egosanto.customerservice.repository.CustomerRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class CustomerServiceApp {

    public static void main(String[] args) {

        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(CustomerServiceApp.class, args);

        CustomerRepository customerRepository =
                configurableApplicationContext.getBean(CustomerRepository.class);

        Customer customer1 = new Customer("Toni", "Makaroni", "Achener Weg 34","50667", "Köln", "toni.m@gmail.com");
        customerRepository.save(customer1);
        Customer customer2 = new Customer("John", "Doe", "Goliam Str 5","50435", "Düsseldorf", "john.d@gmail.com");
        customerRepository.save(customer2);
        Customer customer3 = new Customer("Tom", "of Finland", "Goliat Weg 34","50477", "Bonn", "tomof.h@gmail.com");
        customerRepository.save(customer3);
        Customer customer4 = new Customer("Tobias", "Klein", "Riehler Str 34","57735", "Berlin", "tobi.k@gmail.com");
        customerRepository.save(customer4);
        Customer customer5 = new Customer("Axel", "Buddy", "Otto Weg 34","50675", "Ham", "axel.b@gmail.com");
        customerRepository.save(customer5);
    }
}
